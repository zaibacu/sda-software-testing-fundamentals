package shop;

import java.util.ArrayList;
import java.util.List;

public class Order {
    private List<OrderLine> orderLines = new ArrayList<>();

    public void addProduct(Product product, int qty){
        this.orderLines.add(new OrderLine(product, qty));
    }

    public boolean isValid(String birthDate){
        for(OrderLine line : orderLines){
            if(!line.getProduct().isValid(birthDate)){
                return false;
            }
        }
        return true;
    }

    public float totalSum(){
        float sum = 0;
        for(OrderLine line : orderLines){
            sum += line.sum();
        }
        return sum;
    }

    public List<OrderLine> orderLines(){
        return orderLines;
    }
}
