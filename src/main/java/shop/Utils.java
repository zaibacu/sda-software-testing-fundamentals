package shop;

import java.time.LocalDate;
import java.time.Period;

public class Utils {
    public static int getAgeFromDate(String dateStr, DateGenerator dateGenerator){
        LocalDate now = dateGenerator.today();
        LocalDate given = LocalDate.parse(dateStr);
        return Period.between(given, now).getYears();
    }

    public static int getAgeFromDate(String dateStr){
        return getAgeFromDate(dateStr, () -> LocalDate.now());
    }
}
