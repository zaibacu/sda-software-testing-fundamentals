package shop;

import payment_processing.PaymentProcessor;

public class App {
    public static void main(String[] args){
        Order order = new Order();
        Product soap = new Product("Nivea", 1.5f);
        Product shampoo = new Product("Head & Shoulders", 5.0f);
        Product vodka = new Product("Vodka", 6.0f, 21);
        Product cocaCola = new Product("Coca-Cola", 1.0f);

        order.addProduct(soap, 6);
        order.addProduct(shampoo, 78);
        order.addProduct(vodka, 312);
        order.addProduct(cocaCola, 10);

        PaymentProcessor paymentProcessor = new PaymentProcessor();

        double change = paymentProcessor.processOrder(order);
        if(change != 0) {
            System.out.printf("Change left: %.2f EUR", change);
        }
    }
}
