package shop;

public class OrderLine {
    private Product product;
    private int qty;

    public OrderLine(Product product, int qty) {
        this.product = product;
        this.qty = qty;
    }

    public Product getProduct() {
        return product;
    }

    public int getQty() {
        return qty;
    }

    public float sum(){
        return product.getPrice() * qty;
    }
}
