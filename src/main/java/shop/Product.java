package shop;

public class Product {
    private String name;
    private float price;
    private int ageLimit;

    public Product(String name, float price){
        this.name = name;
        this.price = price;
        this.ageLimit = 0;
    }

    public Product(String name, float price, int ageLimit){
        this.name = name;
        this.price = price;
        this.ageLimit = ageLimit;
    }

    public float getPrice(){
        return price;
    }

    public String getName(){
        return name;
    }

    public boolean isValid(String birthDate){
        if(ageLimit == 0){
            return true;
        }
        int personAge = Utils.getAgeFromDate(birthDate);
        return personAge >= ageLimit;
    }
}
