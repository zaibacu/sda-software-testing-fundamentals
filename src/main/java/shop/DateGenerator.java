package shop;

import java.time.LocalDate;

public interface DateGenerator {
    LocalDate today();
}
