public class RomanNumerals {
    public static String convert(int number) {
        if(number <= 3){
            StringBuilder sb = new StringBuilder();
            for(int i=0; i<number; i++){
                sb.append("I");
            }
            return sb.toString();
        }

        if(number == 4)
            return "IV";

        if(number == 5)
            return "V";

        // 6, 7, 8 = V + (I-III)
        if(number < 9){
            return "V" + convert(number - 5);
        }

        if(number == 9)
            return "IX";
        if(number == 10)
            return "X";

        return "X" + convert(number - 10);
    }
}
