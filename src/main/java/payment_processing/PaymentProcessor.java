package payment_processing;

import shop.Order;
import shop.OrderLine;

import java.util.Scanner;

public class PaymentProcessor {
    public double processOrder(Order order){
        Scanner userInput = new Scanner(System.in);
        // 1)
        // + p1 => price * qty
        // + p2 => price * qty
        // + ...
        // + pn => price * qty


        // 2)


        double totalSum = 0;
        for(OrderLine ol : order.orderLines()){
            for(int i = 0; i<ol.getQty(); i++){
                totalSum += ol.getProduct().getPrice();
            }
        }

        while(totalSum > 0) {
            System.out.printf("Sum left to pay: %.2f EUR\n", totalSum);
            System.out.print("> ");
            float payed = userInput.nextFloat();
            totalSum -= payed;
        }

        return totalSum * -1;
    }
}
