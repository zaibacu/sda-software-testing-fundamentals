package shop;

import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class UtilsTest {
    @Test
    public void testAgeFromDate(){
        int age = Utils.getAgeFromDate("2000-01-01");
        int expectedAge = 21;

        assertEquals(expectedAge, age);
    }

    @Test
    public void testAgeFromDateToday(){
        int age = Utils.getAgeFromDate("2000-06-14", () -> LocalDate.of(2021, 6, 13));
        int expectedAge = 20;

        assertEquals(expectedAge, age);
    }
}
