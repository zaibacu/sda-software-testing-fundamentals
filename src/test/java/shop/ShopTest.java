package shop;

import org.assertj.core.api.Assertions;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ShopTest {
    private static Order testOrder = new Order();

    @BeforeClass
    public static void initOrder(){
        Product soap = new Product("Nivea", 1.5f);
        Product shampoo = new Product("Head & Shoulders", 5.0f);
        Product vodka = new Product("Vodka", 6.0f, 21);

        testOrder.addProduct(soap, 1);
        testOrder.addProduct(shampoo, 2);
        testOrder.addProduct(vodka, 1);
    }

    @Test
    public void testTotalSum(){
        Assertions.assertThat(testOrder.totalSum()).isEqualTo(17.5f);
    }

    @Test
    public void testIsValid(){
        Assertions
                .assertThat(testOrder.isValid("2005-01-01"))
                .withFailMessage("Don't sell to minors")
                .isFalse();
        Assertions
                .assertThat(testOrder.isValid("2000-01-01"))
                .isTrue();
    }

    @Test
    public void testProductName(){
        assertEquals("Nivea", testOrder.orderLines().get(0).getProduct().getName());
    }
}
