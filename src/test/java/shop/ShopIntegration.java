package shop;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import payment_processing.PaymentProcessor;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class ShopIntegration {
    private static final InputStream systemIn = System.in;
    private static final PrintStream systemOut = System.out;

    private static ByteArrayInputStream testIn;
    private static ByteArrayOutputStream testOut;

    public static PaymentProcessor processor = new PaymentProcessor();

    public static void passData(String data){
        testIn = new ByteArrayInputStream(data.getBytes());
        System.setIn(testIn);
    }

    private String getOutput() {
        return testOut.toString();
    }

    @BeforeClass
    public static void setState(){
        testOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOut));
    }

    @AfterClass
    public static void restoreState(){
        System.setIn(systemIn);
        System.setOut(systemOut);
    }

    @Test
    public void testPaymentProcessorIntegration(){
        Order order = new Order();
        Product soap = new Product("Nivea", 1.5f);
        Product shampoo = new Product("Head & Shoulders", 5.0f);
        Product vodka = new Product("Vodka", 6.0f, 21);
        Product cocaCola = new Product("Coca-Cola", 1.0f);

        order.addProduct(soap, 6);
        order.addProduct(shampoo, 78);
        order.addProduct(vodka, 312);
        order.addProduct(cocaCola, 10);

        passData("2000\n1000\n");
        assertEquals(3000 - order.totalSum(), processor.processOrder(order), 0.005);
    }
}
