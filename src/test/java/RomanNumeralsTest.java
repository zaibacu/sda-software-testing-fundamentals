import org.assertj.core.api.Assertions;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;


public class RomanNumeralsTest {
    @Test
    public void testBasics(){
        assertEquals("I", RomanNumerals.convert(1));
        assertEquals("II", RomanNumerals.convert(2));
        assertEquals("III", RomanNumerals.convert(3));
    }

    @Test
    public void testAdvanced(){
        assertEquals("IV", RomanNumerals.convert(4));
        assertEquals("V", RomanNumerals.convert(5));
        assertEquals("VII", RomanNumerals.convert(7));
        assertEquals("VIII", RomanNumerals.convert(8));
    }

    @Test
    public void testMoreAdvanced(){
        assertEquals("IX", RomanNumerals.convert(9));
        assertEquals("X", RomanNumerals.convert(10));
        assertEquals("XII", RomanNumerals.convert(12));
        assertEquals("XVIII", RomanNumerals.convert(18));

        Assertions.assertThat(RomanNumerals.convert(18)).isEqualTo("XVIII");
    }
}
